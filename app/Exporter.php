<?php

namespace App;

use App\DTO\FileDto;
use App\Illuminate\StorageParams;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Exporter
{
    public function __construct(
        protected StorageParams $storageParams,
        protected string $filePath
    )
    {
    }

    public function export() : array
    {
        $data = $this->storageParams->data;

        return ['data' => $data->all()];
    }

    public function  convertToFiles(array $data): array {
        $type = $this->storageParams->params->get('export_type');
        $delimiter = $this->storageParams->params->get('delimiter', ' ');

        if ($type === 'file_txt') {
            $resultFile =uniqid('', true) . '.txt';

            $handle = fopen( $this->filePath . '/' . $resultFile, 'a+');

            if (!empty($data)) {
                $headerArray = (array)$data[0];
                $headerStr = implode($delimiter, array_keys($headerArray)) . PHP_EOL;
                fprintf($handle, rtrim($headerStr, $delimiter));

                // Write data rows
                foreach ($data as $item) {
                    $str = '';
                    foreach ($item as $value) {
                        $value = is_array($value) ? implode(',', $value) : $value;
                        $str .= $value . $delimiter;
                    }

                    fprintf($handle, rtrim($str, $delimiter) . PHP_EOL);
                }
            }

            fclose($handle);

            $dto = new FileDto(
                $resultFile,
                $resultFile,
                'txt',
                'text/plain',
                'export_file_result'
            );
            return [ $dto->toArray() ];
        } elseif ($type === 'file_excel') {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            if (!empty($data)) {
                $columnIndex = 'A';
                $headerArray = (array)$data[0];
                foreach (array_keys($headerArray) as $key) {
                    $sheet->setCellValue($columnIndex . '1', $key);
                    $columnIndex++;
                }

                // Write data rows
                $rowIndex = 2; // Start from the second row
                foreach ($data as $row) {
                    $columnIndex = 'A';
                    foreach ($row as $cell) {
                        $cellFormat = is_array($cell) ? implode(",", $cell) : $cell;
                        $sheet->setCellValue($columnIndex . $rowIndex, $cellFormat);
                        $columnIndex++;
                    }
                    $rowIndex++;
                }
            }

            $resultFile = uniqid('', true) . '.xlsx';
            $writer = new Xlsx($spreadsheet);
            $writer->save($this->filePath . '/' . $resultFile);

            $dto = new FileDto(
                $resultFile,
                $resultFile,
                'xlsx',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'export_file_result'
            );
            return [$dto->toArray()];
        }

        return [];
    }
}