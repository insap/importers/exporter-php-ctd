<?php

namespace App\Illuminate;

use Illuminate\Support\Collection;

class StorageParams
{
    public Collection $params;
    public Collection $files;
    public Collection $data;

    public function __construct( array $params, array $files, array $data)
    {
        $this->params = collect($params);
        $this->files = collect($files);
        $this->data = collect($data);
    }
}